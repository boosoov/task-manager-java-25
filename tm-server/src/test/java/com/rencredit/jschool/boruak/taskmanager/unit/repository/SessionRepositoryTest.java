package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.SessionRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class SessionRepositoryTest {

    @Test
    public void testAdd() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        Assert.assertTrue(sessionRepository.getListDTO().isEmpty());
        @NotNull final SessionDTO session1 = new SessionDTO(123L, "123", "signature123");
        sessionRepository.add(session1);
        @NotNull final SessionDTO sessionFromRepository = sessionRepository.getListDTO().get(0);
        Assert.assertNotNull(sessionFromRepository);
    }

    @Test
    public void testRemove() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();
        final int sizeList = sessionRepository.getListDTO().size();
        @NotNull final SessionDTO session1 = new SessionDTO(123L, "123", "signature123");
        sessionRepository.add(session1);

        sessionRepository.remove(session1);
        sessionRepository.remove(session1);
        Assert.assertEquals(sizeList, sessionRepository.getListDTO().size());
    }

    @Test
    public void testGetList() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();

        Assert.assertFalse(sessionRepository.getListDTO().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();

        Assert.assertFalse(sessionRepository.getListDTO().isEmpty());
        sessionRepository.clearAll();
        Assert.assertTrue(sessionRepository.getListDTO().isEmpty());
    }

    @Test
    public void testLoadCollection() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        @NotNull final Collection<SessionDTO> sessions = new ArrayList<>();
        @NotNull final SessionDTO session1 = new SessionDTO(1L, "1", "signature1");
        sessions.add(session1);
        @NotNull final SessionDTO session2 = new SessionDTO(2L, "2", "signature2");
        sessions.add(session2);
        @NotNull final SessionDTO session3 = new SessionDTO(3L, "3", "signature3");
        sessions.add(session3);

        Assert.assertTrue(sessionRepository.getListDTO().isEmpty());
        sessionRepository.load(sessions);
        Assert.assertEquals(3, sessionRepository.getListDTO().size());
    }

    @Test
    public void testLoadVararg() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        @NotNull final SessionDTO session1 = new SessionDTO(1L, "1", "signature1");
        @NotNull final SessionDTO session2 = new SessionDTO(2L, "2", "signature2");
        @NotNull final SessionDTO session3 = new SessionDTO(3L, "3", "signature3");

        Assert.assertTrue(sessionRepository.getListDTO().isEmpty());
        sessionRepository.load(session1, session2, session3);
        Assert.assertEquals(3, sessionRepository.getListDTO().size());
    }

    @Test
    public void testMergeOne() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        @NotNull final SessionDTO session1 = new SessionDTO(123L, "123", "signature123");
        sessionRepository.merge(session1);

        Assert.assertEquals(1, sessionRepository.getListDTO().size());
    }

    @Test
    public void testMergeCollection() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        @NotNull final Collection<SessionDTO> sessions = new ArrayList<>();
        @NotNull final SessionDTO session1 = new SessionDTO(1L, "1", "signature1");
        sessions.add(session1);
        @NotNull final SessionDTO session2 = new SessionDTO(2L, "2", "signature2");
        sessions.add(session2);
        @NotNull final SessionDTO session3 = new SessionDTO(3L, "3", "signature3");
        sessions.add(session3);

        Assert.assertTrue(sessionRepository.getListDTO().isEmpty());
        sessionRepository.merge(sessions);
        Assert.assertEquals(3, sessionRepository.getListDTO().size());
    }

    @Test
    public void testMergeVararg() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        @NotNull final SessionDTO session1 = new SessionDTO(1L, "1", "signature1");
        @NotNull final SessionDTO session2 = new SessionDTO(2L, "2", "signature2");
        @NotNull final SessionDTO session3 = new SessionDTO(3L, "3", "signature3");

        Assert.assertTrue(sessionRepository.getListDTO().isEmpty());
        sessionRepository.merge(session1, session2, session3);
        Assert.assertEquals(3, sessionRepository.getListDTO().size());
    }

    @Test
    public void testFindAll() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();

        @NotNull final List<SessionDTO> sessionList = sessionRepository.getListDTO();
        Assert.assertEquals(sessionList.size(), 5);
    }

    @Test
    public void testRemoveByUserId() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();

        sessionRepository.removeByUserId("1");
        sessionRepository.removeByUserId("7");
        @NotNull final List<SessionDTO> sessionList = sessionRepository.getListDTO();
        Assert.assertEquals(5, sessionList.size());
    }

    @Test
    public void testRemoveBySessionDTO() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();
        @NotNull final int sizeList = sessionRepository.getListDTO().size();
        @NotNull final SessionDTO session = new SessionDTO(1L, "1", "signature1");
        sessionRepository.add(session);

//        final boolean sessionRemoved = sessionRepository.removeBySession(session);
//        Assert.assertTrue(sessionRemoved);
        Assert.assertEquals(sizeList, sessionRepository.getListDTO().size());
    }

    @Test
    public void testRemoveNotExist() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();

        sessionRepository.removeByUserId("000");
        @NotNull final SessionDTO session = new SessionDTO(16746L, "1345", "signature45345");
//        final boolean sessionRemovedByIndex = sessionRepository.removeBySession(session);
//        Assert.assertFalse(sessionRemovedByIndex);
    }

    @Test
    public void testFindById() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();
        @NotNull final SessionDTO session = new SessionDTO(123L, "123", "signature123");
        sessionRepository.add(session);

        @Nullable final String id = session.getId();
        @Nullable final Session sessionFind = sessionRepository.findById(id);
        Assert.assertEquals(session, sessionFind);
    }

    @Test
    public void testFindNotExist() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();

        @Nullable final Session sessionFindById = sessionRepository.findById("123");
        Assert.assertNull(sessionFindById);

    }

    @Test
    public void testContains() {
        @NotNull final SessionRepository sessionRepository = getFullSessionRepository();
        @NotNull final SessionDTO session = new SessionDTO(123L, "123", "signature123");
        sessionRepository.add(session);

        @Nullable final String id = session.getId();
        final boolean isContainTrue = sessionRepository.contains(id);
        Assert.assertTrue(isContainTrue);

        final boolean isContainFalse = sessionRepository.contains("12345");
        Assert.assertFalse(isContainFalse);
    }

    public SessionRepository getFullSessionRepository() {
        @NotNull final SessionRepository sessionRepository = new SessionRepository();
        @NotNull final SessionDTO session1 = new SessionDTO(1L, "1", "signature1");
        sessionRepository.add(session1);
        @NotNull final SessionDTO session2 = new SessionDTO(2L, "2", "signature2");
        sessionRepository.add(session2);
        @NotNull final SessionDTO session3 = new SessionDTO(3L, "3", "signature3");
        sessionRepository.add(session3);
        @NotNull final SessionDTO session4 = new SessionDTO(4L, "4", "signature4");
        sessionRepository.add(session4);
        @NotNull final SessionDTO session5 = new SessionDTO(5L, "5", "signature5");
        sessionRepository.add(session5);
        return sessionRepository;
    }

}
