package com.rencredit.jschool.boruak.taskmanager.unit.util;

import com.rencredit.jschool.boruak.taskmanager.entity.Session;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyHashLineException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class SignatureUtilTest {

    @Test
    public void signGetObjectTest() {
        @NotNull final Session session = new Session();
        session.setId("12345");
        Assert.assertEquals(
                "2242a5610c7e0ca2193a6c92ca55a745",
                SignatureUtil.sign(session, "asdf", 77)
        );
    }

    @Test
    public void signGetStringTest() throws EmptyHashLineException {
        Assert.assertEquals(
                "aaddad0267c5257e8032d07835e2d0ec",
                SignatureUtil.sign("value", "asdf", 77)
        );
    }

}
