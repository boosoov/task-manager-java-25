package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class TaskRepositoryTest {

    @Test
    public void testAdd() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        Assert.assertTrue(taskRepository.getListDTO().isEmpty());
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name", "description");
        taskRepository.add(task1);
        @NotNull final TaskDTO taskFromRepository = taskRepository.getListDTO().get(0);
        Assert.assertNotNull(taskFromRepository);
        Assert.assertNotNull(taskFromRepository.getId());
        Assert.assertNotNull(taskFromRepository.getName());
    }

    @Test
    public void testRemove() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        final int sizeList = taskRepository.getListDTO().size();
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name1", "description");
        taskRepository.add(task1);

        taskRepository.remove(task1);
        taskRepository.remove(task1);
        Assert.assertEquals(sizeList, taskRepository.getListDTO().size());
    }

    @Test
    public void testGetList() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        Assert.assertFalse(taskRepository.getListDTO().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        Assert.assertFalse(taskRepository.getListDTO().isEmpty());
        taskRepository.clearAll();
        Assert.assertTrue(taskRepository.getListDTO().isEmpty());
    }

    @Test
    public void testLoadCollection() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Collection<TaskDTO> tasks = new ArrayList<>();
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name1", "description");
        tasks.add(task1);
        @NotNull final TaskDTO task2 = new TaskDTO("1", "name2", "description");
        tasks.add(task2);
        @NotNull final TaskDTO task3 = new TaskDTO("1", "name3", "description");
        tasks.add(task3);

        Assert.assertTrue(taskRepository.getListDTO().isEmpty());
        taskRepository.load(tasks);
        Assert.assertEquals(3, taskRepository.getListDTO().size());
    }

    @Test
    public void testLoadVararg() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name1", "description");
        @NotNull final TaskDTO task2 = new TaskDTO("1", "name2", "description");
        @NotNull final TaskDTO task3 = new TaskDTO("1", "name3", "description");

        Assert.assertTrue(taskRepository.getListDTO().isEmpty());
        taskRepository.load(task1, task2, task3);
        Assert.assertEquals(3, taskRepository.getListDTO().size());
    }

    @Test
    public void testMergeOne() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name1", "description");
        taskRepository.merge(task1);

        Assert.assertEquals(1, taskRepository.getListDTO().size());
    }

    @Test
    public void testMergeCollection() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final Collection<TaskDTO> tasks = new ArrayList<>();
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name1", "description");
        tasks.add(task1);
        @NotNull final TaskDTO task2 = new TaskDTO("1", "name2", "description");
        tasks.add(task2);
        @NotNull final TaskDTO task3 = new TaskDTO("1", "name3", "description");
        tasks.add(task3);

        Assert.assertTrue(taskRepository.getListDTO().isEmpty());
        taskRepository.merge(tasks);
        Assert.assertEquals(3, taskRepository.getListDTO().size());
    }

    @Test
    public void testMergeVararg() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name1", "description");
        @NotNull final TaskDTO task2 = new TaskDTO("1", "name2", "description");
        @NotNull final TaskDTO task3 = new TaskDTO("1", "name3", "description");

        Assert.assertTrue(taskRepository.getListDTO().isEmpty());
        taskRepository.merge(task1, task2, task3);
        Assert.assertEquals(3, taskRepository.getListDTO().size());
    }

    @Test
    public void testFindAll() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        @NotNull final List<TaskDTO> taskList = taskRepository.getListDTO();
        Assert.assertEquals(taskList.size(), 5);
    }

    @Test
    public void testClearByUserId() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        taskRepository.clearByUserId("1");
        taskRepository.clearByUserId("7");
        @NotNull final List<TaskDTO> taskList = taskRepository.getListDTO();
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void testFindAllByUserId() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        @NotNull final List<TaskDTO> taskList = taskRepository.findAllByUserIdDTO("1");
        Assert.assertEquals(3, taskList.size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final TaskDTO task = new TaskDTO("1", "name", "description");
        taskRepository.add(task);

        @NotNull final String id = task.getId();
        @Nullable final TaskDTO taskFind = taskRepository.findOneDTOById("1", id);
        Assert.assertEquals(task, taskFind);
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final TaskDTO task = new TaskDTO("1", "name", "description");
        taskRepository.add(task);

        @Nullable final TaskDTO taskFind = taskRepository.findOneDTOByIndex("1", 3);
        Assert.assertEquals(task, taskFind);
    }

    @Test
    public void testFindOneByName() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final TaskDTO task = new TaskDTO("1", "name777", "description");
        taskRepository.add(task);

        @Nullable final TaskDTO taskFind = taskRepository.findOneDTOByName("1", "name777");
        Assert.assertEquals(task, taskFind);
    }

    @Test
    public void testFindNotExist() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        @Nullable final TaskDTO taskFindById = taskRepository.findOneDTOById("1", "000");
        Assert.assertNull(taskFindById);
        @Nullable final TaskDTO taskFindByIndex = taskRepository.findOneDTOByIndex("1", 10);
        Assert.assertNull(taskFindByIndex);
        @Nullable final TaskDTO taskFindByName = taskRepository.findOneDTOByName("1", "000");
        Assert.assertNull(taskFindByName);
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final int sizeList = taskRepository.getListDTO().size();
        @NotNull final TaskDTO task = new TaskDTO("1", "name", "description");
        taskRepository.add(task);

        @NotNull final String id = task.getId();
        taskRepository.removeOneById("1", id);
        Assert.assertEquals(sizeList, taskRepository.getListDTO().size());
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final int sizeList = taskRepository.getListDTO().size();
        @NotNull final TaskDTO task = new TaskDTO("1", "name", "description");
        taskRepository.add(task);

        taskRepository.removeOneByIndex("1", 3);
        Assert.assertEquals(sizeList, taskRepository.getListDTO().size());
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();
        @NotNull final int sizeList = taskRepository.getListDTO().size();
        @NotNull final TaskDTO task = new TaskDTO("1", "name777", "description");
        taskRepository.add(task);

        taskRepository.removeOneByName("1", "name777");
        Assert.assertEquals(sizeList, taskRepository.getListDTO().size());
    }

    @Test
    public void testRemoveNotExist() {
        @NotNull final TaskRepository taskRepository = getFullTaskRepository();

        taskRepository.removeOneById("1", "000");
        taskRepository.removeOneByIndex("1", 10);
        taskRepository.removeOneByName("1", "000");
    }

    public TaskRepository getFullTaskRepository() {
        @NotNull final TaskRepository taskRepository = new TaskRepository();
        @NotNull final TaskDTO task1 = new TaskDTO("1", "name1", "description");
        taskRepository.add(task1);
        @NotNull final TaskDTO task2 = new TaskDTO("1", "name2", "description");
        taskRepository.add(task2);
        @NotNull final TaskDTO task3 = new TaskDTO("1", "name3", "description");
        taskRepository.add(task3);
        @NotNull final TaskDTO task4 = new TaskDTO("2", "name4", "description");
        taskRepository.add(task4);
        @NotNull final TaskDTO task5 = new TaskDTO("7", "name5", "description");
        taskRepository.add(task5);
        return taskRepository;
    }

}
