package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class ProjectRepositoryTest {

    @Test
    public void testAdd() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        Assert.assertTrue(projectRepository.getListDTO().isEmpty());
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name", "description");
        projectRepository.add(project1);
        @NotNull final ProjectDTO projectFromRepository = projectRepository.getListDTO().get(0);
        Assert.assertNotNull(projectFromRepository);
        Assert.assertNotNull(projectFromRepository.getId());
        Assert.assertNotNull(projectFromRepository.getName());
    }

    @Test
    public void testRemove() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        final int sizeList = projectRepository.getListDTO().size();
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name1", "description");
        projectRepository.add(project1);

        projectRepository.remove(project1);
        projectRepository.remove(project1);
        Assert.assertEquals(sizeList, projectRepository.getListDTO().size());
    }

    @Test
    public void testGetList() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        Assert.assertFalse(projectRepository.getListDTO().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        Assert.assertFalse(projectRepository.getListDTO().isEmpty());
        projectRepository.clearAll();
        Assert.assertTrue(projectRepository.getListDTO().isEmpty());
    }

    @Test
    public void testLoadCollection() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Collection<ProjectDTO> projects = new ArrayList<>();
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name1", "description");
        projects.add(project1);
        @NotNull final ProjectDTO project2 = new ProjectDTO("1", "name2", "description");
        projects.add(project2);
        @NotNull final ProjectDTO project3 = new ProjectDTO("1", "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectRepository.getListDTO().isEmpty());
        projectRepository.load(projects);
        Assert.assertEquals(3, projectRepository.getListDTO().size());
    }

    @Test
    public void testLoadVararg() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name1", "description");
        @NotNull final ProjectDTO project2 = new ProjectDTO("1", "name2", "description");
        @NotNull final ProjectDTO project3 = new ProjectDTO("1", "name3", "description");

        Assert.assertTrue(projectRepository.getListDTO().isEmpty());
        projectRepository.load(project1, project2, project3);
        Assert.assertEquals(3, projectRepository.getListDTO().size());
    }

    @Test
    public void testMergeOne() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name1", "description");
        projectRepository.merge(project1);

        Assert.assertEquals(1, projectRepository.getListDTO().size());
    }

    @Test
    public void testMergeCollection() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final Collection<ProjectDTO> projects = new ArrayList<>();
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name1", "description");
        projects.add(project1);
        @NotNull final ProjectDTO project2 = new ProjectDTO("1", "name2", "description");
        projects.add(project2);
        @NotNull final ProjectDTO project3 = new ProjectDTO("1", "name3", "description");
        projects.add(project3);

        Assert.assertTrue(projectRepository.getListDTO().isEmpty());
        projectRepository.merge(projects);
        Assert.assertEquals(3, projectRepository.getListDTO().size());
    }

    @Test
    public void testMergeVararg() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name1", "description");
        @NotNull final ProjectDTO project2 = new ProjectDTO("1", "name2", "description");
        @NotNull final ProjectDTO project3 = new ProjectDTO("1", "name3", "description");

        Assert.assertTrue(projectRepository.getListDTO().isEmpty());
        projectRepository.merge(project1, project2, project3);
        Assert.assertEquals(3, projectRepository.getListDTO().size());
    }

    @Test
    public void testFindAll() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        @NotNull final List<ProjectDTO> projectList = projectRepository.getListDTO();
        Assert.assertEquals(projectList.size(), 5);
    }

    @Test
    public void testClearByUserId() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        projectRepository.clearByUserId("1");
        projectRepository.clearByUserId("7");
        @NotNull final List<ProjectDTO> projectList = projectRepository.getListDTO();
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void testFindAllByUserId() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        @NotNull final List<ProjectDTO> projectList = projectRepository.findAllByUserIdDTO("1");
        Assert.assertEquals(3, projectList.size());
    }

    @Test
    public void testFindOneById() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final ProjectDTO project = new ProjectDTO("1", "name", "description");
        projectRepository.add(project);

        @NotNull final String id = project.getId();
        @Nullable final ProjectDTO projectFind = projectRepository.findOneDTOById("1", id);
        Assert.assertEquals(project, projectFind);
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final ProjectDTO project = new ProjectDTO("1", "name", "description");
        projectRepository.add(project);

        @Nullable final ProjectDTO projectFind = projectRepository.findOneDTOByIndex("1", 3);
        Assert.assertEquals(project, projectFind);
    }

    @Test
    public void testFindOneByName() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final ProjectDTO project = new ProjectDTO("1", "name777", "description");
        projectRepository.add(project);

        @Nullable final ProjectDTO projectFind = projectRepository.findOneDTOByName("1", "name777");
        Assert.assertEquals(project, projectFind);
    }

    @Test
    public void testFindNotExist() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        @Nullable final ProjectDTO projectFindById = projectRepository.findOneDTOById("1", "000");
        Assert.assertNull(projectFindById);
        @Nullable final ProjectDTO projectFindByIndex = projectRepository.findOneDTOByIndex("1", 10);
        Assert.assertNull(projectFindByIndex);
        @Nullable final ProjectDTO projectFindByName = projectRepository.findOneDTOByName("1", "000");
        Assert.assertNull(projectFindByName);
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final int sizeList = projectRepository.getListDTO().size();
        @NotNull final ProjectDTO project = new ProjectDTO("1", "name", "description");
        projectRepository.add(project);

        @NotNull final String id = project.getId();
        projectRepository.removeOneById("1", id);
        Assert.assertEquals(sizeList, projectRepository.getListDTO().size());
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final int sizeList = projectRepository.getListDTO().size();
        @NotNull final ProjectDTO project = new ProjectDTO("1", "name", "description");
        projectRepository.add(project);

        projectRepository.removeOneByIndex("1", 3);
        Assert.assertEquals(sizeList, projectRepository.getListDTO().size());
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();
        @NotNull final int sizeList = projectRepository.getListDTO().size();
        @NotNull final ProjectDTO project = new ProjectDTO("1", "name777", "description");
        projectRepository.add(project);

        projectRepository.removeOneByName("1", "name777");
        Assert.assertEquals(sizeList, projectRepository.getListDTO().size());
    }

    @Test
    public void testRemoveNotExist() {
        @NotNull final ProjectRepository projectRepository = getFullProjectRepository();

        projectRepository.removeOneById("1", "000");
        projectRepository.removeOneByIndex("1", 10);
        projectRepository.removeOneByName("1", "000");
    }

    private ProjectRepository getFullProjectRepository() {
        @NotNull final ProjectRepository projectRepository = new ProjectRepository();
        @NotNull final ProjectDTO project1 = new ProjectDTO("1", "name1", "description");
        projectRepository.add(project1);
        @NotNull final ProjectDTO project2 = new ProjectDTO("1", "name2", "description");
        projectRepository.add(project2);
        @NotNull final ProjectDTO project3 = new ProjectDTO("1", "name3", "description");
        projectRepository.add(project3);
        @NotNull final ProjectDTO project4 = new ProjectDTO("2", "name4", "description");
        projectRepository.add(project4);
        @NotNull final ProjectDTO project5 = new ProjectDTO("7", "name5", "description");
        projectRepository.add(project5);
        return projectRepository;
    }

}
