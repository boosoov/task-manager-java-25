package com.rencredit.jschool.boruak.taskmanager.unit.repository;

import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Category(UnitTestCategory.class)
public class UserRepositoryTest {

    @Test
    public void testAdd() {
        @NotNull final UserRepository userRepository = new UserRepository();
        Assert.assertTrue(userRepository.getListEntity().isEmpty());
        @NotNull final User user1 = new User("login", "passwordHash");
        userRepository.addUser(user1);
        @NotNull final User userFromRepository = userRepository.getListEntity().get(0);
        Assert.assertNotNull(userFromRepository);
    }

    @Test
    public void testRemove() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        final int sizeList = userRepository.getListEntity().size();
        @NotNull final UserDTO user1 = new UserDTO("login", "passwordHash");
        userRepository.add(user1);

        userRepository.removeByUser(user1);
        Assert.assertEquals(sizeList, userRepository.getListEntity().size());
    }

    @Test
    public void testGetList() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        Assert.assertFalse(userRepository.getListEntity().isEmpty());
    }

    @Test
    public void testClearAll() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        Assert.assertFalse(userRepository.getListEntity().isEmpty());
        userRepository.clearAll();
        Assert.assertTrue(userRepository.getListEntity().isEmpty());
    }

    @Test
    public void testLoadCollection() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final Collection<UserDTO> users = new ArrayList<>();
        @NotNull final UserDTO user1 = new UserDTO("login1", "passwordHash1");
        users.add(user1);
        @NotNull final UserDTO user2 = new UserDTO("login2", "passwordHash2");
        users.add(user2);
        @NotNull final UserDTO user3 = new UserDTO("login3", "passwordHash3");
        users.add(user3);

        Assert.assertTrue(userRepository.getListEntity().isEmpty());
        userRepository.load(users);
        Assert.assertEquals(3, userRepository.getListEntity().size());
    }

    @Test
    public void testLoadVararg() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final UserDTO user1 = new UserDTO("login1", "passwordHash1");
        @NotNull final UserDTO user2 = new UserDTO("login2", "passwordHash2");
        @NotNull final UserDTO user3 = new UserDTO("login3", "passwordHash3");

        Assert.assertTrue(userRepository.getListEntity().isEmpty());
        userRepository.load(user1, user2, user3);
        Assert.assertEquals(3, userRepository.getListEntity().size());
    }

    @Test
    public void testMergeOne() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final UserDTO user1 = new UserDTO("login", "passwordHash");
        userRepository.merge(user1);

        Assert.assertEquals(1, userRepository.getListEntity().size());
    }

    @Test
    public void testMergeCollection() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final Collection<UserDTO> users = new ArrayList<>();
        @NotNull final UserDTO user1 = new UserDTO("login1", "passwordHash1");
        users.add(user1);
        @NotNull final UserDTO user2 = new UserDTO("login2", "passwordHash2");
        users.add(user2);
        @NotNull final UserDTO user3 = new UserDTO("login3", "passwordHash3");
        users.add(user3);

        Assert.assertTrue(userRepository.getListEntity().isEmpty());
        userRepository.merge(users);
        Assert.assertEquals(3, userRepository.getListEntity().size());
    }

    @Test
    public void testMergeVararg() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final UserDTO user1 = new UserDTO("login1", "passwordHash1");
        @NotNull final UserDTO user2 = new UserDTO("login2", "passwordHash2");
        @NotNull final UserDTO user3 = new UserDTO("login3", "passwordHash3");

        Assert.assertTrue(userRepository.getListEntity().isEmpty());
        userRepository.merge(user1, user2, user3);
        Assert.assertEquals(3, userRepository.getListEntity().size());
    }

    @Test
    public void testFindAll() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        @NotNull final List<User> userList = userRepository.getListEntity();
        Assert.assertEquals(userList.size(), 5);
    }

    @Test
    public void testFindById() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final UserDTO user = new UserDTO("login", "passwordHash");
        userRepository.add(user);

        @NotNull final String id = user.getId();
        @Nullable final UserDTO userFind = userRepository.findByIdDTO(id);
        Assert.assertEquals(user, userFind);
    }

    @Test
    public void testFindByLogin() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final UserDTO user = new UserDTO("login", "passwordHash");
        userRepository.add(user);

        @Nullable final UserDTO userFind = userRepository.findByIdDTO("login");
        Assert.assertEquals(user, userFind);
    }

    @Test
    public void testFindNotExist() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        @Nullable final User userFindById = userRepository.findByIdEntity("000");
        Assert.assertNull(userFindById);
        @Nullable final User userFindByIndex = userRepository.findByIdEntity("000");
        Assert.assertNull(userFindByIndex);
    }

    @Test
    public void testRemoveById() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final int sizeList = userRepository.getListEntity().size();
        @NotNull final UserDTO user = new UserDTO("login", "passwordHash");
        userRepository.add(user);

        @NotNull final String id = user.getId();
        userRepository.removeById(id);
        Assert.assertEquals(sizeList, userRepository.getListEntity().size());
    }

    @Test
    public void testRemoveByLogin() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final int sizeList = userRepository.getListEntity().size();
        @NotNull final UserDTO user = new UserDTO("login", "passwordHash");
        userRepository.add(user);

        userRepository.removeByLogin("login");
        Assert.assertEquals(sizeList, userRepository.getListEntity().size());
    }

    @Test
    public void testRemoveByUser() {
        @NotNull final UserRepository userRepository = getFullUserRepository();
        @NotNull final int sizeList = userRepository.getListEntity().size();
        @NotNull final UserDTO user = new UserDTO("login", "passwordHash");
        userRepository.add(user);

        userRepository.removeByUser(user);
        Assert.assertEquals(sizeList, userRepository.getListEntity().size());
    }

    @Test
    public void testRemoveNotExist() {
        @NotNull final UserRepository userRepository = getFullUserRepository();

        userRepository.removeById("000");
        userRepository.removeByLogin("000");
        @NotNull final UserDTO user = new UserDTO("000", "passwordHash");
        userRepository.removeByUser(user);
    }

    public UserRepository getFullUserRepository() {
        @NotNull final UserRepository userRepository = new UserRepository();
        @NotNull final UserDTO user1 = new UserDTO("login1", "passwordHash1");
        userRepository.add(user1);
        @NotNull final UserDTO user2 = new UserDTO("login2", "passwordHash2");
        userRepository.add(user2);
        @NotNull final UserDTO user3 = new UserDTO("login3", "passwordHash3");
        userRepository.add(user3);
        @NotNull final UserDTO user4 = new UserDTO("login4", "passwordHash4");
        userRepository.add(user4);
        @NotNull final UserDTO user5 = new UserDTO("login5", "passwordHash5");
        userRepository.add(user5);
        return userRepository;
    }

}
