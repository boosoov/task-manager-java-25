package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IDataBasePropertyService;
import com.rencredit.jschool.boruak.taskmanager.api.service.IPropertyService;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService, IDataBasePropertyService {

    @NotNull
    private final static String NAME = "/application.properties";

    @NotNull
    private final Properties properties = new Properties();

    public PropertyService() {
        init();
    }

    @Override
    public boolean init() {
        @NotNull final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        try {
            properties.load(inputStream);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String propertyHost = properties.getProperty("server.host");
        @NotNull final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String propertyPort = properties.getProperty("server.port");
        @NotNull final String envPort = System.getProperty("server.port");
        @NotNull String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionSalt() {
        return properties.getProperty("session.salt");
    }

    @NotNull
    @Override
    public Integer getSessionCycle() {
        return Integer.parseInt(properties.getProperty("session.cycle"));
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return properties.getProperty("jdbcDriver");
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return properties.getProperty("db.host");
    }

    @NotNull
    @Override
    public String getJdbcUsername() {
        return properties.getProperty("db.login");
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return properties.getProperty("db.password");
    }

}
