package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IAuthEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Fail;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Result;
import com.rencredit.jschool.boruak.taskmanager.dto.status.Success;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    private IAuthService authService;

    public AuthEndpoint() {
    }

    public AuthEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        authService = serviceLocator.getAuthService();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    @WebMethod
    public SessionDTO logIn(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) throws EmptyUserException, EmptyHashLineException, EmptyLoginException, DeniedAccessException, EmptyPasswordException {
        @NotNull final SessionDTO session = sessionService.open(login, password);
        return session;
    }

    @NotNull
    @Override
    @WebMethod
    public Result logOut(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        try {
            sessionService.close(session);
            return new Success();
        } catch (final Exception e) {
            return new Fail(e);
        }
    }

    @Nullable
    @Override
    @WebMethod
    public String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        return session.getUserId();
    }

    @Override
    @WebMethod
    public void checkRoles(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "roles", partName = "roles") final Role[] roles
    ) throws DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptyRoleException, EmptySessionException {
        sessionService.validate(session);
        authService.checkRoles(session, roles);
    }

    @Override
    @WebMethod
    public void registrationLoginPassword(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration(login, password);
    }

    @Override
    @WebMethod
    public void registrationLoginPasswordFirstName(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "firstName", partName = "firstName") final String firstName
    ) throws EmptyHashLineException, EmptyEmailException, EmptyFirstNameException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException {
        authService.registration(login, password, firstName);
    }

    @Override
    @WebMethod
    public void registrationLoginPasswordRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role") final Role role
    ) throws EmptyPasswordException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyLoginException, EmptyUserException, DeniedAccessException, NotExistUserException, EmptyIdException, EmptyUserIdException, EmptySessionException {
        if(!authService.checkRoles(session, roles())) throw new DeniedAccessException();
        authService.registration(login, password, role);
    }

}
