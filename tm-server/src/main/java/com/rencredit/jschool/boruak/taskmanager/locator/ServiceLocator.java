package com.rencredit.jschool.boruak.taskmanager.locator;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.repository.IProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ISessionRepository;
import com.rencredit.jschool.boruak.taskmanager.api.repository.ITaskRepository;
import com.rencredit.jschool.boruak.taskmanager.api.repository.IUserRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.*;
import com.rencredit.jschool.boruak.taskmanager.repository.ProjectRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.SessionRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.TaskRepository;
import com.rencredit.jschool.boruak.taskmanager.repository.UserRepository;
import com.rencredit.jschool.boruak.taskmanager.service.*;
import com.rencredit.jschool.boruak.taskmanager.util.EntityManagerFactoryUtil;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public class ServiceLocator implements IServiceLocator {

    @NotNull
    private final IUserService userService = new UserService();

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ITaskService taskService = new TaskService();

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final IDomainService domainService = new DomainService(projectService, taskService, userService);

    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IStorageService storageService = new StorageService(this);


    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public IStorageService getStorageService() {
        return storageService;
    }

}
