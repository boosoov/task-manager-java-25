package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.api.service.ISessionService;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AbstractEndpoint {

    @NotNull
    IServiceLocator serviceLocator;

    @NotNull
    ISessionService sessionService;

    @NotNull
    IAuthService authService;

    @Nullable
    public Role[] roles() {
        return null;
    }

    public AbstractEndpoint() {
    }

    public AbstractEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.sessionService = serviceLocator.getSessionService();
        this.authService = serviceLocator.getAuthService();
    }

}
