package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyElementsException;

import java.io.IOException;

public interface IStorageService {

    boolean clearBase64() throws IOException;

    boolean loadBase64() throws IOException, ClassNotFoundException, EmptyElementsException;

    boolean saveBase64() throws IOException;

    boolean clearBinary() throws IOException;

    boolean loadBinary() throws IOException, ClassNotFoundException, EmptyElementsException;

    boolean saveBinary() throws IOException;

    boolean clearJson() throws IOException;

    boolean loadJson() throws IOException, EmptyElementsException;

    boolean saveJson() throws IOException;

    boolean clearXml() throws IOException;

    boolean loadXml() throws IOException, EmptyElementsException;

    boolean saveXml() throws IOException;

}
