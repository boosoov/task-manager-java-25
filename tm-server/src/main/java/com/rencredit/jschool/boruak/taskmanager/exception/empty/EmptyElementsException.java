package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyElementsException extends AbstractException {

    public EmptyElementsException() {
        super("Error! Elements is empty...");
    }

}
