package com.rencredit.jschool.boruak.taskmanager.endpoint;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.IProjectEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.locator.IServiceLocator;
import com.rencredit.jschool.boruak.taskmanager.api.service.IProjectService;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    IProjectService projectService;

    public ProjectEndpoint() {
    }

    public ProjectEndpoint(
            @NotNull IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        projectService = serviceLocator.getProjectService();
    }

    @Override
    @WebMethod
    public void createProjectName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createProjectNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws EmptyNameException, EmptyDescriptionException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjectByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return projectService.findAllByUserId(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return projectService.findOneDTOById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException {
        sessionService.validate(session);
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        return projectService.findOneByName(session.getUserId(), name);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws EmptyIdException, EmptyNameException, EmptyUserIdException, EmptyProjectException, DeniedAccessException, EmptyDescriptionException {
        sessionService.validate(session);
        return projectService.updateProjectById(session.getUserId(), id, name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) throws IncorrectIndexException, EmptyNameException, EmptyUserIdException, EmptyProjectException, DeniedAccessException, EmptyDescriptionException {
        sessionService.validate(session);
        return projectService.updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) throws EmptyIdException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        projectService.removeOneById(session.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) throws EmptyUserIdException, IncorrectIndexException, DeniedAccessException {
        sessionService.validate(session);
        projectService.removeOneByIndex(session.getUserId(), index);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) throws EmptyNameException, EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        projectService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void clearAllUserProjects(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws EmptyUserIdException, DeniedAccessException {
        sessionService.validate(session);
        projectService.clearByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void clearAllProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException, EmptyUserException, EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyRoleException {
        sessionService.validate(session);
        projectService.clearAll();
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> getListProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) throws DeniedAccessException {
        sessionService.validate(session);
        return projectService.getList();
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> getTasksByProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId
    ) throws DeniedAccessException, EmptyProjectIdException {
        sessionService.validate(session);
        return serviceLocator.getTaskService().findAllByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public void attachTaskToProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId,
            @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId
    ) throws DeniedAccessException, EmptyTaskIdException, EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyProjectIdException {
        sessionService.validate(session);
        serviceLocator.getTaskService().attachTaskToProject(session.getUserId(), taskId, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public void detachTaskFromProject(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId,
            @Nullable @WebParam(name = "taskId", partName = "taskId") final String taskId
    ) throws DeniedAccessException, EmptyTaskIdException, EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyProjectIdException {
        sessionService.validate(session);
        serviceLocator.getTaskService().detachTaskToProject(session.getUserId(), taskId, projectId);
    }
}
