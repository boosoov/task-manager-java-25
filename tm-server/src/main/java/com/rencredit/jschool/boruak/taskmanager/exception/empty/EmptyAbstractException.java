package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class EmptyAbstractException extends AbstractException {

    public EmptyAbstractException() {
        super("Error! Abstract is empty...");
    }

}
