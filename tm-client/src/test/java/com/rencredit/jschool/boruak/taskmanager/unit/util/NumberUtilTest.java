package com.rencredit.jschool.boruak.taskmanager.unit.util;

import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.util.NumberUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class NumberUtilTest {

    @Test
    public void testFormatBytes() {
        Assert.assertEquals("53 MB", NumberUtil.formatBytes(56356345));
    }

}
