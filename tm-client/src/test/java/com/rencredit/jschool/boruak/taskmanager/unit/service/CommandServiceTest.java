package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.api.repository.ICommandRepository;
import com.rencredit.jschool.boruak.taskmanager.api.service.ICommandService;
import com.rencredit.jschool.boruak.taskmanager.command.AbstractCommand;
import com.rencredit.jschool.boruak.taskmanager.command.info.HelpCommand;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyCommandException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import com.rencredit.jschool.boruak.taskmanager.repository.CommandRepository;
import com.rencredit.jschool.boruak.taskmanager.service.CommandService;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.Map;

@Category(UnitTestCategory.class)
public class CommandServiceTest {

    @NotNull ICommandRepository commandRepository;
    @NotNull ICommandService commandService;

    @Before
    public void init() {
        commandRepository = new CommandRepository();
        commandService = new CommandService(commandRepository);
    }

    @Test(expected = EmptyNameException.class)
    public void testNegativePutCommandWithoutName() throws EmptyCommandException, EmptyNameException {
        commandService.putCommand(null, new HelpCommand());
    }

    @Test(expected = EmptyCommandException.class)
    public void testNegativePutCommandWithoutCommand() throws EmptyCommandException, EmptyNameException {
        commandService.putCommand("Help", null);
    }

    @Test
    public void testPutCommand() throws EmptyCommandException, EmptyNameException {
        Assert.assertEquals(0, commandService.getCommands().length);
        commandService.putCommand("Help", new HelpCommand());
        Assert.assertEquals(1, commandService.getCommands().length);
    }

    @Test
    public void testGetCommands() throws EmptyCommandException, EmptyNameException {
        commandService.putCommand("Help", new HelpCommand());
        @NotNull final String[] commands = commandService.getCommands();
        Assert.assertEquals("help: Display terminal command.", commands[0]);
    }

    @Test
    public void testGetArgs() throws EmptyCommandException, EmptyNameException {
        @NotNull final AbstractCommand command = new HelpCommand();
        commandService.putCommand("Help", new HelpCommand());
        @NotNull final String[] commands = commandService.getArgs();
        Assert.assertEquals("-h: Display terminal command.", commands[0]);
    }

    @Test
    public void testGetTerminalCommands() throws EmptyCommandException, EmptyNameException {
        @NotNull final AbstractCommand command = new HelpCommand();
        commandService.putCommand("Help", new HelpCommand());
        Map<String, AbstractCommand> commands = commandService.getTerminalCommands();
        @NotNull final AbstractCommand commandFromService = commands.get(new String("Help"));
        Assert.assertEquals(command.description(), commandFromService.description());
    }

}
