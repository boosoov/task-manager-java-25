package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyRoleException extends AbstractClientException {

    public EmptyRoleException() {
        super("Error! Role is empty...");
    }

}
