package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptySessionException extends AbstractClientException {

    public EmptySessionException() {
        super("Error! Session is empty...");
    }

}
